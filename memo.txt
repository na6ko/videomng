* 記録
./mame.exe thndrx2j -record tc2.inp

* 再生 -> MNG と WAV 作成
./mame.exe thndrx2j -playback tc2_11_54.inp -mngwrite tc2.mng -wavwrite tc2.wav

* MNG 分離
mng_extract.c 参照

10000 の png ファイル作成 2 並列 -> 29.2 s + 29.5 s
20000 の png ファイル作成単体 -> 53.0s 
* 連番png->lossless movie without audio
#lossless H.264 11.7 MB
/g/ffmpeg-4.3.1/bin/ffmpeg -r 60 -i %04d.png -c:v libx264 -preset ultrafast -crf 0 -f mp4 output.mp4
#264MB
/g/ffmpeg-4.3.1/bin/ffmpeg -r 60 -i %04d.png -c:v huffyuv -pix_fmt rgb24 output.avi
#161MB
/g/ffmpeg-4.3.1/bin/ffmpeg -r 60 -i %04d.png -c:v huffyuv -pix_fmt yuv422p output.avi

* 連結
/g/ffmpeg-4.3.1/bin/ffmpeg -y -f concat -safe 0 -i list -c copy nn.mp4

 = sprintf("%s -r %8.5f -i %s%%06d.png -c:v libx264 -preset ultrafast -crf 0 -f mp4  %s", FFMPEG, videofps, temppath, destmp4)
c = sprintf("%s -f concat -safe 0 -i list -c copy %s", FFMPEG, videofile)

/g/ffmpeg-4.3.1/bin/ffmpeg -y -f concat -safe 0 -i list -c:v libx264 -preset ultrafast -crf 0 -f mp4 hoge.mp4

* 音を足す
/g/ffmpeg-4.3.1/bin/ffmpeg -y -i v.mp4 -i start1-1_end5-4/tc2.wav -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 va.mp4

/g/ffmpeg-4.3.1/bin/ffmpeg -y \
-ss 5000 -i start1-1_end5-4/tc2.wav -t 333.3333 -c aac a0015.aac

/g/ffmpeg-4.3.1/bin/ffmpeg -y \
-i n0015.mp4 \
-i a0015.aac \
-c:v copy -c:a copy -map 0:v:0 -map 1:a:0 na0015.mp4

* sfx
mng: 47245 frames
wav: 38,436,480 samples, 48_000 Hz

* tc2 start1-1 end5-4 音ズレ
連結
あり Duration: 01:31:52.44, start: 0.000000, bitrate: 4575 kb/s
なし Duration: 01:31:52.16, start: 0.000000, bitrate: 4576 kb/s

330730 frames @60 Hz -> 55512.1666...
連結すると 17 frame 程度挿入されるようだ.
$ r 'p (330730.0+16)/60'
5512.433333333333

$ r 'p (330730.0+17)/60'
5512.45

/g/ffmpeg-4.3.1/bin/ffmpeg -y -i n0000.mp4 -i n0001.mp4 -i n0002.mp4 -i n0003.mp4 -i n0004.mp4 -i n0005.mp4 -i n0006.mp4 -i n0007.mp4 -i n0008.mp4 -i n0009.mp4 -i n0010.mp4 -i n0011.mp4 -i n0012.mp4 -i n0013.mp4 -i n0014.mp4 -i n0015.mp4 -i n0016.mp4 -c:v libx264 -preset ultrafast -crf 0 -f mp4  -filter_complex "concat=n=17:v=1:a=0" hoge.mp4

