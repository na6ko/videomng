#define _FILE_OFFSET_BITS 64
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#define BUFSIZE (0x2000000)

/*
$ ./mng_extract.exe /g/encoding/test/tc2.mng 0 4000 @
$ cat pngoffset
0x0000000030 0x0001f0e303 0x00044f9895
10547

4000 + 4000 + 2519 -> 10519
*/

struct buf{
	struct {
		FILE *const f;
		off_t offset, length;
	}file;
	struct {
		uint8_t *const top;
		uint8_t *buf;
		size_t offset, length;
	}buf;
	struct {
		uint64_t frame_count;
		off_t offset;
	}total;
};
static inline void buf_update(struct buf *t, size_t s)
{
	t->buf.buf += s;
	t->buf.offset += s;
	t->buf.length -= s;
	t->total.offset += s;
	if(t->buf.length >= 0x20000){
		return;
	}
	if(t->file.length == 0){
		return;
	}
	memcpy(t->buf.top, t->buf.buf, t->buf.length);
	t->buf.buf = t->buf.top;
	size_t l = BUFSIZE - t->buf.length;
	size_t buf_length = BUFSIZE;
	if(t->file.length < l){
		l = t->file.length;
		buf_length = t->buf.length + t->file.length;
	}
	fread(t->buf.top + t->buf.length, 1, l, t->file.f);
	t->buf.offset = 0;
	t->buf.length = buf_length;
	t->file.length -= l;
	t->file.offset += l;
}
static inline uint32_t unpack_be32(const uint8_t *buf)
{
	uint32_t l = buf[0] << 24;
	l |= buf[1] << 16;
	l |= buf[2] << 8;
	l |= buf[3];
	return l;
}
struct pngdata{
	uint8_t top[0x40000], *buf;
	int offset;
};
static inline enum CHUNKRESULT{
	CHUNK_NOTFOUND,
	CHUNK_OK,
	CHUNK_IEND,
	CHUNK_MEND
}chunk_read(struct buf *t, const char *header, struct pngdata *p)
{
	//unpack be32
	uint32_t l = t->buf.buf[0] << 24;
	l |= t->buf.buf[1] << 16;
	l |= t->buf.buf[2] << 8;
	l |= t->buf.buf[3];
	//cmpare header
	const uint8_t *const h = t->buf.buf + 4;
	enum CHUNKRESULT r = CHUNK_OK;
	//use memcmp(), not strncmp()
	if(l == 0 && memcmp(h, "IEND", 4) == 0){
		r = CHUNK_IEND;
	}else if(l == 0 && memcmp(h, "MEND", 4) == 0){
		r = CHUNK_MEND;
	}else if(memcmp(h, header, 4) != 0){
		printf("chuck %s is not found", header);
		fflush(stdout);
		return CHUNK_NOTFOUND;
	}
	l += 4 + 4 + 4; //length + header + data + CRC32
	if(p != NULL){
		assert(sizeof(p->buf) < (p->offset + l));
		memcpy(p->buf, t->buf.buf, l);
		p->buf += l;
		p->offset += l;
	}
	buf_update(t, l); 
	return r;
}
/*static void fm(const char *str)
{
	FILE *f = fopen("offset", "w");
	fputs(str, f);
	fclose(f);
}*/
static const uint8_t pngheader[] = {
	0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a
};

static enum CHUNKRESULT png_extract(struct buf *t, struct pngdata *p, const uint32_t pngnum, const char *destprefix)
{
	enum CHUNKRESULT r;
	for(int i = 0; i < pngnum; i++){
		if(p != NULL){
			p->buf = p->top + sizeof(pngheader);
			p->offset = sizeof(pngheader);
		}
		r = chunk_read(t, "IHDR", p);
		if(r == CHUNK_MEND){
			break;
		}
		assert(r == CHUNK_OK);
		r = chunk_read(t, "IDAT", p);
		assert(r == CHUNK_OK);
		do{
			r = chunk_read(t, "tEXt", p);
			assert(r != CHUNK_NOTFOUND);
		}while(r != CHUNK_IEND);
		t->total.frame_count += 1;
		if(p != NULL){
			char pngfilename[0x40];
			snprintf(pngfilename, sizeof(pngfilename), "%s%06d.png", destprefix, i);
			FILE *d = fopen(pngfilename, "wb");
			assert(d);
			size_t n = fwrite(p->top, 1, p->offset, d);
			assert(n == p->offset);
			fclose(d);
		}
		if(t->buf.length <= 0){
			assert(0);
			break;
		}
	}
	return r;
}
static void mng_extract(struct buf *t, const off_t seekoffset, uint32_t pngnum, const char *destprefix)
{
	if(seekoffset > t->file.length){
		puts("seekoffset > t->file.length");
		return;
	}
	if((t->file.length - seekoffset) < BUFSIZE){
		t->buf.length = t->file.length - seekoffset;
	}
	fseeko(t->file.f, seekoffset, SEEK_SET);
	t->file.offset = seekoffset;
	t->file.length -= seekoffset;
	fread(t->buf.buf, 1, t->buf.length, t->file.f);
	
	int fps = 0;
	if(seekoffset == 0){
		static const uint8_t mngfirst[8]= {
			0x8a, 0x4d, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a
		};
		if(memcmp(mngfirst, t->buf.buf, sizeof(mngfirst)) != 0){
			puts("mng header is not found");
			return;
		}
		buf_update(t, sizeof(mngfirst));
		fps = unpack_be32(t->buf.buf + 4 + 4 + 4 * 2);
		if(chunk_read(t, "MHDR", NULL) != CHUNK_OK){
			return;
		}
		//align for fps
		pngnum -= pngnum % fps;
	}
	if(destprefix != NULL){
		struct pngdata *const p = malloc(sizeof(struct pngdata));
		assert(p != NULL);
		memcpy(p->top, pngheader, sizeof(pngheader));
		png_extract(t, p, pngnum, destprefix);
		free(p);
		printf("total:%I64d, offset:0x%I64x\n", t->total.frame_count, t->total.offset);
	}else{
		FILE *f = fopen("pngoffset", "w");
		do{
			fprintf(f, "0x%010I64x ", t->total.offset);
		}while(png_extract(t, NULL, pngnum, destprefix) != CHUNK_MEND);
		fprintf(f, "\n%u\n%u\n%I64d\n", fps, pngnum, t->total.frame_count);
		fclose(f);
	}
}

static off_t filesize_get(const char *filename)
{
	const int d = open(filename, O_RDONLY);
	if(d == -1){
		return -1;
	}
	FILE *const f = fdopen(d, "rb");
	assert(f != NULL);
	struct stat s;
	int r = fstat(d, &s);
	assert(r == 0);
	r++;
	fclose(f);
	return s.st_size;
}
int main(int c, const char **v)
{
	if(c < 5){
		puts("[mng filename] [offset] [maxnum] [destprefix]");
		return -1;
	}
	off_t filesize = filesize_get(v[1]);
	const off_t seekoffset = strtoull(v[2], NULL, 0);
	const uint32_t maxnum = strtoul(v[3], NULL, 0);
	const char *destprefix = v[4];
	if(strncmp(v[4], "@", 3) == 0){
		destprefix = NULL;
	}

	FILE *const f = fopen(v[1], "rb");
	uint8_t *buf = malloc(BUFSIZE);
	struct buf t = {
		.file = {.f = f, .offset = 0, .length = filesize},
		.buf = {.top = buf, .buf = buf, .offset = 0, .length = BUFSIZE},
		.total = {.frame_count = 0, .offset = seekoffset}
	};
	mng_extract(&t, seekoffset, maxnum, destprefix);
	free(buf);
	fclose(f);
	return 0;
}
