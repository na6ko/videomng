def chunk_read(f, png, chunk)
	d = f.read(4)
	png.concat(d.unpack('C*'))
	l = d.unpack('N1')[0]
	c = f.read(4)
	png.concat(c.unpack('C*'))
	if c != 'IEND' && c != chunk
		puts chunk + ' is not found'
		return 0
	end
	d = f.read(l + 4)
	png.concat(d.unpack('C*'))
	return c == 'IEND' ? 2 : 1
end
def png_extract(f, dest)
	png = [0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a]
	['IHDR', 'IDAT'].each{|t|
		if chunk_read(f, png, t) == 0
			exit
		end
	}
	while true
		case chunk_read(f, png, 'tEXt')
		when 0
			exit
		when 1
			next
		when 2
			break
		end
	end
	s = File.open(dest, 'wb')
	s.write(png.pack('C*'))
	s.close
end
def mng_extract(f, dest)
	first = f.read(8).unpack("C8")
	if first != [0x8a, 0x4d, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a]
		puts 'offset 0 error'
		exit
	end
	#MHDR
	l = f.read(4).unpack('N1')[0]
	f.read(4+l+4) #MHDR + data = CRC
	(60*60).times{|i|
		png_extract(f, sprintf("%s/%04d.png", dest, i))
	}
end
begin
	f = File.open(ARGV[0], 'rb')
	destpath = File.dirname(ARGV[0])
	mng_extract(f, destpath)
	f.close
end
