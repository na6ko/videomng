def pngdividenum(mngfile, wavfile, temppath)
	c = 'df ' + File.dirname(temppath)
	drive_aki = `df #{File.dirname(temppath)}`.split(/\n/)[1].split(' ')[3].to_i * 1024

	waveheader = File.open(wavfile, 'rb').read(0x2C).unpack('A4V1A8V1v2V2v2A4V')
	wavfreq = waveheader[6]
	wavsample = waveheader.last / waveheader[8]
	wavseconds = wavsample.to_f / wavfreq

	guessed_videoframes = wavseconds * 60
	guessed_avgpngfilesize = File.size(mngfile) / guessed_videoframes
	
	pngnum = drive_aki * 0.55 / guessed_avgpngfilesize
	[pngnum, 20000].min
end
FFMPEG = "g:/ffmpeg-4.3.1/bin/ffmpeg.exe -loglevel error -y"
begin
	if ARGV.size < 4
		puts "[mngfile] [wavfile] [temppath] [destpath]"
		exit
	end
	mngfile = ARGV.shift
	wavfile = ARGV.shift
	temppath = ARGV.shift
	destpath = ARGV.shift
	#pngfilenum = 18000
	#pngfilenum = 14000
	pngfilenum = pngdividenum(mngfile, wavfile, temppath)

	c = sprintf("./mng_extract.exe %s %d %d @", mngfile, 0, pngfilenum)
	system(c)
	if $?.exitstatus != 0
		exit
	end
	f = File.open('./pngoffset')
	pngoffset = f.gets.strip.split(' ')
	p videofps = f.gets.to_i
	p pngfilenum = f.gets.to_i
	f.close
	i = 0
	partedmp4 = []
	partedlastframepng = sprintf("%s%06d.png", temppath, pngfilenum - 1)
	pngoffset.each{|offset|
		c = sprintf("./mng_extract.exe %s %s %d %s", mngfile, offset, pngfilenum, temppath)
		p c
		system(c)
		if $?.exitstatus != 0
			exit
		end
		if File.exist?(partedlastframepng)
			File.delete(partedlastframepng)
		end
		destmp4 = sprintf("%sn%04d.mp4", destpath, i)
		partedmp4 << destmp4
		#generate a video file from pngs
		c = sprintf("%s -r %d -i %s%%06d.png -c:v libx264 -preset ultrafast -crf 0 -f mp4 %s", FFMPEG, videofps, temppath, destmp4)
		p c
		system(c)
		Dir.glob(temppath + "??????.png"){|p|
			File.delete(p)
		}
		i += 1
	}

	videofile = partedmp4[0]
	if partedmp4.size != 1
		f = File.open("list", 'w')
		partedmp4.each{|t|
			f.printf("file %s\n", t)
		}
		f.close
		videofile = destpath + "/v.mp4"
		c = sprintf("%s -f concat -safe 0 -i list -c copy -f mp4 %s", FFMPEG, videofile)
		p c
		system(c)
		#partedmp4.each{|t|
		#	File.delete(t)
		#}
	end
	c = sprintf("%s -i %s -i %s -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 %sva.mp4", FFMPEG, videofile, wavfile, destpath)
	p c
	system(c)
	if File.exist?(videofile)
		File.delete(videofile)
	end
end
